class demo {
  int? _x;
  String? str;
  double? _sal;

  demo(this._x, this.str, this._sal);

  int? get getX {
    return _x;
  }

  double? get getSal {
    return _sal;
  }

  String? get getStr {
    return str;
  }
}

void main() {
  demo obj = new demo(1, "kailas", 12.34);

  print(obj.getStr);
  print(obj.getSal);
  print(obj.getX);
}
