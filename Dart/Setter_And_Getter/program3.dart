class demo{
	int? _x;
	String? name;
	double? _sal;

demo(this._x, this.name, this._sal);

	int? get getX => _x;
	String? get getName => name;	
	double? get getSal => _sal;

}

void main(){
	
demo obj=new demo(1,"kailas",12.32);

	print(obj.getX);
	print(obj.getName);
	print(obj.getSal);

}