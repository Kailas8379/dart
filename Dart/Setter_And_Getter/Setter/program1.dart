class demo {
  int? _x;
  String? str;
  double? _sal;

  demo(this._x, this.str, this._sal);

  void SetX(int x) {
    _x = x;
  }

  void SetStr(String name) {
    str = name;
  }

  void SetSal(double Sal) {
    _sal = Sal;
  }
}

void main() {
  demo obj = new demo(23, "solanke", 232.12);

  obj.SetStr("kailas");
  obj.SetX(12);
  obj.SetSal(123.21);
}
