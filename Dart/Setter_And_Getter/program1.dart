class Demo {
  int? _x;
  String? str;
  double? _sal;

  Demo(this._x, this.str, this._sal);
  int? getX() {
    return _x;
  }

  double? getSal() {
    return _sal;
  }
}

void main() {
  Demo obj1 = new Demo(10, "kailas", 1.34);
  print(obj1.getX());
  print(obj1.getSal());
  print(obj1.str);
}
