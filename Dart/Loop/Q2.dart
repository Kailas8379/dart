//2. Write a program to print the first 100 numbers

import 'dart:io';

void main() {
  for (int i = 1; i <= 100; i++) {
    stdout.write("  $i");
  }
}
