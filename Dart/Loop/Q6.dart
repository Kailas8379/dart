//6. Write a program to print reverse from 100-1

import 'dart:io';

void main() {
  for (int i = 100; i >= 1; i--) {
    stdout.write(" $i");
  }
}
