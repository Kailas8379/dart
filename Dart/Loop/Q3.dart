//3. Write a program to print the first ten, 3 digit number

import 'dart:io';

void main() {
  int x = 100;

  for (int i = 0; i < 3; i++) {
    stdout.write(" $x");
    x++;
  }
}
