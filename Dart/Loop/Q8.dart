// 8. Write a program to print a table of 12 in reverse order

import 'dart:io';

void main() {
  for (int i = 10; i >= 1; i--) {
    int table = i * 12;
    stdout.write(" $table");
  }
}
