//Program 1: Write a program to print a table of 2
//Output: 2 4 6 8 10 12 14 16 18 20

import 'dart:io';

void main() {
  for (int i = 1; i <= 10; i++) {
    int table = i * 2;
    stdout.write(" $table");
  }
}
