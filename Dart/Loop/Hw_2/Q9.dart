//Program 9: Write a program to reverse the given number.
//Input: 942111423
//Output: 324111249

import 'dart:io';

void main() {
  int numb = 8379;
  int rev = 0;
  while (numb != 0) {
    int rem = numb % 10;
    rev = rev * 10 + rem;
    numb = numb ~/ 10;
  }
  stdout.write(rev);
}
