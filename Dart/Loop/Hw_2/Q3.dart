//Program 3: Write a program to count the digits of the given
//number.
//Input: 942111423
//Output: count of digits = 9

import 'dart:io';

void main() {
  int numb = 837987;

  int cnt = 0;

  int rem = 1;

  while (numb != 0) {
    rem = numb % 10;
    cnt++;
    numb = numb ~/ 10;
  }
  stdout.write(cnt);
}
