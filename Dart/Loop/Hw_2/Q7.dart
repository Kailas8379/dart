//Program 7: Write a program that takes a number, if the number
//is even print that number in reverse order, or if the number is
//odd print that number in reverse order by difference two

import 'dart:io';

void main() {
  int x = 6;

  if (x % 2 == 0) {
    for (int i = x; i >= 1; i--) {
      stdout.write(" $i");
    }
  } else {
    for (int j = x; j >= 1; j--) {
      if (j % 2 != 0) {
        stdout.write(" $j");
      }
    }
  }
}
