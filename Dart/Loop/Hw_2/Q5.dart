//Program 5: Write a program to print the square of even
//digits of the given number.
//Input: 942111423
//Output: 4 16 4 16

import 'dart:io';

void main() {
  int numb = 9421;

  int cnt = 0;

  int rem = 1;

  while (numb != 0) {
    rem = numb % 10;
    if (rem % 2 == 0) {
      int seq = rem * rem;
      stdout.write(" $seq");
    }
    numb = numb ~/ 10;
  }
}
