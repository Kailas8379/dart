//Program 4: Write a program to count the Odd digits of the
//given number.
//Input: 942111423
//Output: count of odd digits = 5

void main() {
  int numbe = 1243;
  int cnt = 0;
  int rem = 1;

  while (numbe != 0) {
    rem = numbe % 10;
    if (rem % 2 != 0) {
      cnt++;
    }
    numbe = numbe ~/ 10;
  }
  print(cnt);
}
