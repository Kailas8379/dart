//Program 6: Write a program to print the sum of all even
//numbers and the multiplication of odd numbers between 1 to 10.
//Output: sum of even numbers between 1 to 10 = 30
//Multiplication of odd numbers between 1 to 10 = 945

import 'dart:io';

void main() {
  int sum = 0;
  int mul = 1;
  for (int i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      sum = sum + i;
    } else {
      mul = mul * i;
    }
  }
  stdout.write("Multiplication of odd numbers between 1 to 10 = $mul\n");
  stdout.write("sum of even numbers between 1 to 10 = $sum");
}
