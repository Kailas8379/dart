//Program 10: Write a program to check whether the number is a
//Palindrome number or not. (2332)
//Output: 2332 is a palindrome number

import 'dart:io';

void main() {
  int numb = 8379;
  int num3 = numb;
  int rev = 0;
  while (numb != 0) {
    int rem = numb % 10;
    rev = rev * 10 + rem;
    numb = numb ~/ 10;
  }
  if (rev == num3) {
    stdout.write("$num3 is a palindrom number");
  } else {
    stdout.write("is not a palindrom number");
  }
  stdout.write(rev);
}
