// 7. Write a program to print a table of 12

import 'dart:io';

void main() {
  for (int i = 1; i <= 10; i++) {
    int table = i * 12;
    stdout.write(" $table");
  }
}
