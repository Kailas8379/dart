// 9. Write a program to print the sum of the first 10 numbers

import 'dart:io';

void main() {
  int sum = 0;

  for (int i = 1; i <= 10; i++) {
    sum = sum + i;
  }
  stdout.write(" $sum");
}
