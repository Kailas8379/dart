//5. Write a program to print odd numbers 1-50

import 'dart:io';

void main() {
  for (int i = 1; i <= 50; i++) {
    if (i % 2 != 0) {
      stdout.write(" $i");
    }
  }
}
