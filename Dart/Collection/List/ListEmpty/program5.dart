void main() {
  var prolang = List.empty(growable: true);
  prolang.add("Cpp");
  prolang.add("Java");
  prolang.add("Python");
  prolang.add("Java");

  print(prolang);
  print(prolang[2]);

  print(prolang.elementAt(3));
  print(prolang.getRange(0, 3));

  print(prolang.indexOf("Python"));
//indexWhere
  print(prolang.indexWhere((lang) => lang.startsWith("P")));
}
