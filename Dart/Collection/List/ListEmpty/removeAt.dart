void main() {
  var prolang = List.empty(growable: true);
  prolang.add("Cpp");
  prolang.add("Java");
  prolang.add("Python");
  prolang.add("Java");

  print(prolang);

  var lang = ["ReactJs", "Spring", "Flutter"];

  prolang.addAll(lang);

  print(prolang);

  prolang.insert(3, "Dart");
  print(prolang);
  prolang.insertAll(3, ["Go", "Swift"]);
  print(prolang);

//RepaceRange
  prolang.replaceRange(3, 7, {"Dart", "Swift"});

  print(prolang);
  prolang.remove("ReactJs");

  print(prolang);

  prolang.add("Dart");

  prolang.remove("Dart");
  print(prolang);

  prolang.removeAt(5);
  print(prolang);
}
