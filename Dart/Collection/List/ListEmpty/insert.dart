void main() {
  var prolang = List.empty(growable: true);
  prolang.add("Cpp");
  prolang.add("Java");
  prolang.add("Python");
  prolang.add("Java");

  print(prolang);

  var lang = ["ReactJs", "Spring", "Flutter"];

  prolang.addAll(lang);

  print(prolang);

  prolang.insert(3, "Dart");
  print(prolang);
}
