import 'dart:io';

void main() {
  var add = (int a, int b) {
    return a + b;
  };

  add(10, 20);

  int x = 10;

  stdout.writeln(add.runtimeType);
  stdout.writeln(x.runtimeType);
}
