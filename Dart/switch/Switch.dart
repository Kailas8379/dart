import 'dart:io';

void main() {
  int x = 4;
  switch (x) {
    case 1:
      stdout.write("one");
    case 2:
      stdout.write("two");
    case 3:
      stdout.write("three");
    default:
      stdout.write("no match");
  }
}
