import 'dart:io';

class Employee {
  int empId = 10;
  String EName = "Ashish";
  double sal = 12.34;

  void empInfo() {
    stdout.writeln(empId);
    stdout.writeln(EName);
    stdout.writeln(sal);
  }
}

void main() {
  Employee obj1 = new Employee();
  obj1.empInfo();

  Employee obj2 = Employee();
  obj2.empInfo();
}
