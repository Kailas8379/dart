import 'dart:io';

class Emp {
  int? eimpID = 1;
  String? eName = "kailas";
  double? sal = 102.43;
  void eInfo() {
    stdout.writeln("EmpId = $eimpID");
    stdout.writeln("emp Name =$eName");
    stdout.writeln("sal is = $sal");
  }
}

void main() {
  Emp obj = new Emp();
  obj.eInfo();

  stdout.writeln("Enter the name  ");
  obj.eName = stdin.readLineSync();

  stdout.writeln("Enter the Emp id  ");
  obj.eimpID = int.parse(stdin.readLineSync()!);

  stdout.writeln("Enter the salary ");
  obj.sal = double.parse(stdin.readLineSync()!);

  stdout.writeln("-----------------------");
  obj.eInfo();
}
