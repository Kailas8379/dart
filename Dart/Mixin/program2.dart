mixin Demoparent {
  void m1() {
    print("In m1-Demoparent");
  }
}

class demo {
  void m2() {
    print("in m2 -Demo");
  }
}

class Demochild extends demo with Demoparent {}

void main() {}
