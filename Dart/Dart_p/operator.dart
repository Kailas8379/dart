void main() {
  var a = 10;
  var b = 20;
  var c = 40;
  var d = 2;
  print(a + b);
  print(b - c);
  print(a * d);
  print(c / d);

  //relation operator
  var g = 100;
  var k = 200;
  print(g == k);
  print(g <= k);
  print(g >= k);
  print(g != k);
  print(g < k);
  print(g > k);
}
