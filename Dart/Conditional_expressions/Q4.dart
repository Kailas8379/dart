/*
Program 4 :
Write a dart program, take a number and print whether it is
positive or negative.
Input: var=5
Output: 5 is a positive number
Input: var=-9
Output: -9 is a negative number
*/

import 'dart:io';

void main(){

int x=-5;

if(x>=0){
stdout.write("$x is a positive number");
}else{
stdout.write("$x is negative number");
}
}
