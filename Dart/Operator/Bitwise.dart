void main() {
  int x = 9;
  int y = 7;

  print(x & y); //AND
  print(x | y); //OR
  print(x ^ y); //XOR
  print(x << 3); //Shift
  print(y >> 3); //Shift Right
}
