void main() {
  int x = 10;
  int y = 8;

  print(x == y); //Equal
  print(x != y); //Not Equal
  print(x > y); //Greater Than
  print(x < y); //Less Than
  print(x >= y); //Greater Than or Equal
  print(x <= y); //Less tha or Equal to
}
