class Demo {
  int x = 10;

  void fun1() {
    print("IN fun");
    print(x);
  }
}

class Demochild extends Demo {
  int x = 20;
  void fun1() {
    print("In fun Demochild");
    print(x);
  }
}

void main() {
  Demo obj1 = new Demo();
  obj1.fun1();
  Demochild obj2 = new Demochild();
  obj2.fun1();
  Demo obj3 = new Demochild();
  obj3.fun1();
}
