class Parent {
  void career() {
    print("Engineering");
  }

  void marry() {
    print("Disha Patani");
  }
}

class child extends Parent {
  void marry() {
    print("Deepika Padukone");
  }

  void profession() {
    print("Software Engineering");
  }
}

void main() {
  Parent pobj = new Parent();
  Pobj.career();
  pobj.career();
  pobj.profession();
}
