class Demo {
  int x = 10;

  void fun1() {
    print("IN fun");
    print(x);
  }
}

class Demochild extends Demo {
  void fun1() {
    print("In fun Demochild");
    print(x);
  }
}

void main() {
  Demo obj1 = new Demo();
  obj1.fun1();
  Demochild obj = new Demochild();
  obj.fun1();
}
