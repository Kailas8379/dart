class Backend {
  String? lang;
  Backend._code(String lang) {
    if (lang == "javaScript") {
      this.lang = "NodeJs";
    } else if (lang == "java") {
      this.lang = "SpringBoot";
    } else {
      this.lang = "NodeJs/SpringBoot";
    }
  }
  factory Backend(String lang) {
    return Backend._code(lang);
  }
}
